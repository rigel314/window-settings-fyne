package wset

import (
	"fmt"
	"reflect"
	"strings"
	"sync/atomic"

	"fyne.io/fyne"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

// SettingConstraint defines a Constraint method that will be called for each struct member and the struct as a whole, if the type implements the interface.
// Constraint is allowed to modify the value.  If ok is false, msg will be displayed near the corresponding input.
type SettingConstraint interface {
	Constraint() (ok bool, msg string)
}

// ConfigScreen creates a fyne element to act as a settings screen, generated from the struct pointer value.
func ConfigScreen(av atomic.Value) fyne.CanvasObject {
	value := av.Load()

	var v reflect.Value

	if reflect.TypeOf(value).Kind() != reflect.Ptr {
		panic(fmt.Sprintf("Unhandled type: %v, only pointers to structs are allowed", reflect.TypeOf(value).Kind()))
	}

	v = reflect.ValueOf(value).Elem()

	if v.Type().Kind() != reflect.Struct {
		panic(fmt.Sprintf("Unhandled pointer type: %v, only pointers to structs are allowed", reflect.TypeOf(value).Kind()))
	}

	vbakp := reflect.New(v.Type())
	vbak := vbakp.Elem()
	vbak.Set(v)

	box := widget.NewVBox()

	applyfns, resetfns := addFields(box, v, v.Type(), nil)

	box.Append(layout.NewSpacer())
	butts := widget.NewHBox(
		widget.NewButton("Apply", func() {
			ok := true
			for _, f := range applyfns {
				ok = ok && f(v)
				if !ok {
					for _, f := range resetfns {
						f(vbak)
					}
					v.Set(vbak)
					break
				}
			}
			if ok {
				vbak.Set(v)
				av.Store(value)
			}
		}),
		widget.NewButton("Reset", func() {
			for _, f := range resetfns {
				f(vbak)
			}
		}),
	)
	box.Append(fyne.NewContainerWithLayout(layout.NewCenterLayout(), butts))
	// TODO: add success/failure indication

	ret := widget.NewScrollContainer(box)

	for _, f := range resetfns {
		f(vbak)
	}

	return ret
}

func addFields(b *widget.Box, v reflect.Value, t reflect.Type, idxs []int) (applyfns []func(v reflect.Value) bool, resetfns []func(v reflect.Value)) {
	applyfns = make([]func(v reflect.Value) bool, 0, v.NumField())
	resetfns = make([]func(v reflect.Value), 0, v.NumField())
	for i := 0; i < v.NumField(); i++ {
		idxs := append(idxs, i)
		e := v.Field(i)
		et := t.Field(i)
		etags := strings.Split(et.Tag.Get("wset"), ";")

		title := et.Name
		// desc := ""
		for _, v := range etags {
			l := strings.SplitN(v, "=", 2)
			if len(l) < 2 {
				continue
			}
			switch l[0] {
			case "title":
				title = l[1]
			case "description":
				// desc = l[1]
			}
		}
		switch e.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			ent := widget.NewEntry()
			resetfns = append(resetfns, func(v reflect.Value) {
				e := v.FieldByIndex(idxs)
				ent.SetText(fmt.Sprint(e.Int()))
			})
			applyfns = append(applyfns, func(v reflect.Value) bool {
				e := v.FieldByIndex(idxs)
				var i int64
				_, err := fmt.Sscan(ent.Text, &i)
				if err != nil {
					return false
				}
				e.SetInt(i)
				if et.Type.Implements(reflect.TypeOf((*SettingConstraint)(nil)).Elem()) {
					ok, msg := e.Interface().(SettingConstraint).Constraint()
					if !ok {
						_ = msg
					}
				}
				return true
			})
			hb := widget.NewHBox(
				widget.NewVBox(
					layout.NewSpacer(),
					widget.NewLabelWithStyle(title+":", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
				),
				ent,
			)
			b.Append(hb)
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			ent := widget.NewEntry()
			resetfns = append(resetfns, func(v reflect.Value) {
				e := v.FieldByIndex(idxs)
				ent.SetText(fmt.Sprint(e.Uint()))
			})
			applyfns = append(applyfns, func(v reflect.Value) bool {
				e := v.FieldByIndex(idxs)
				var i uint64
				_, err := fmt.Sscan(ent.Text, &i)
				if err != nil {
					return false
				}
				e.SetUint(i)
				if et.Type.Implements(reflect.TypeOf((*SettingConstraint)(nil)).Elem()) {
					ok, msg := e.Interface().(SettingConstraint).Constraint()
					if !ok {
						_ = msg
					}
				}
				return true
			})
			hb := widget.NewHBox(
				widget.NewVBox(
					layout.NewSpacer(),
					widget.NewLabelWithStyle(title+":", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
				),
				ent,
			)
			b.Append(hb)
		case reflect.String:
			ent := widget.NewEntry()
			resetfns = append(resetfns, func(v reflect.Value) {
				e := v.FieldByIndex(idxs)
				ent.SetText(e.String())
			})
			applyfns = append(applyfns, func(v reflect.Value) bool {
				e := v.FieldByIndex(idxs)
				e.SetString(ent.Text)
				if et.Type.Implements(reflect.TypeOf((*SettingConstraint)(nil)).Elem()) {
					ok, msg := e.Interface().(SettingConstraint).Constraint()
					if !ok {
						_ = msg
					}
				}
				return true
			})
			hb := widget.NewHBox(
				widget.NewVBox(
					layout.NewSpacer(),
					widget.NewLabelWithStyle(title+":", fyne.TextAlignLeading, fyne.TextStyle{Bold: true}),
				),
				ent,
			)
			b.Append(hb)
		case reflect.Bool:
			ck := widget.NewCheck(title, nil)
			resetfns = append(resetfns, func(v reflect.Value) {
				e := v.FieldByIndex(idxs)
				ck.SetChecked(e.Bool())
			})
			applyfns = append(applyfns, func(v reflect.Value) bool {
				e := v.FieldByIndex(idxs)
				e.SetBool(ck.Checked)
				if et.Type.Implements(reflect.TypeOf((*SettingConstraint)(nil)).Elem()) {
					ok, msg := e.Interface().(SettingConstraint).Constraint()
					if !ok {
						_ = msg
					}
				}
				return true
			})
			hb := widget.NewHBox(ck)
			// if desc != "" {
			// 	e := widget.NewLabel(desc)
			// 	hb.Append(e)
			// }
			b.Append(hb)
		case reflect.Struct:
			box := widget.NewVBox()
			afns, rfns := addFields(box, e, e.Type(), idxs)
			applyfns = append(applyfns, afns...)
			resetfns = append(resetfns, rfns...)
			sp := fyne.NewContainerWithLayout(layout.NewFixedGridLayout(fyne.NewSize(10, 1)))
			b.Append(fyne.NewContainerWithLayout(layout.NewBorderLayout(nil, nil, sp, nil),
				sp,
				widget.NewGroup(title, widget.NewVBox(box, layout.NewSpacer())),
			))
		}
	}
	return
}
